// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

module mus.mojom;

import "cc/ipc/compositor_frame_metadata.mojom";
import "cc/ipc/quads.mojom";
import "cc/ipc/returned_resource.mojom";
import "cc/ipc/transferable_resource.mojom";
import "ui/gfx/geometry/mojo/geometry.mojom";
import "gpu/ipc/common/mailbox_holder.mojom";
import "gpu/ipc/common/sync_token.mojom";

// See src/cc/output/compositor_frame.h.
struct CompositorFrame {
  cc.mojom.CompositorFrameMetadata metadata;
  array<cc.mojom.TransferableResource> resources;
  array<cc.mojom.RenderPass> passes;
};

// A Surface is an interface for receiving CompositorFrame structs. This is a
// separate interface to allow CompositorFrames to be delivered from
// supplementary (not main) threads of a mojo app.
interface Surface {
  // After the submitted frame is drawn for the first time, the receiver will
  // respond to the SubmitFrame message. Clients should use this acknowledgement
  // to ratelimit frame submissions.
  SubmitCompositorFrame(CompositorFrame frame) => ();
};

interface SurfaceClient {
  ReturnResources(array<cc.mojom.ReturnedResource> resources);
};
