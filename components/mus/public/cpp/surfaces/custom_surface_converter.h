// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COMPONENTS_MUS_PUBLIC_CPP_SURFACES_CUSTOM_SURFACE_CONVERTER_H_
#define COMPONENTS_MUS_PUBLIC_CPP_SURFACES_CUSTOM_SURFACE_CONVERTER_H_

#include "cc/ipc/quads.mojom.h"
#include "components/mus/public/interfaces/compositor_frame.mojom.h"

namespace cc {
class CompositorFrameMetadata;
class RenderPass;
class SharedQuadState;
}  // namespace cc

namespace mojo {

// Classes that inherit from this converter can override the default behavior
// for converting a mojo::SurfaceDrawState to something cc understands.
class CustomSurfaceConverter {
 public:
  virtual bool ConvertSurfaceDrawQuad(
      const cc::mojom::DrawQuadPtr& input,
      const cc::CompositorFrameMetadata& metadata,
      cc::SharedQuadState* sqs,
      cc::RenderPass* render_pass) = 0;

 protected:
  virtual ~CustomSurfaceConverter() {}
};

} // namespace mojo

#endif  // COMPONENTS_MUS_PUBLIC_CPP_SURFACES_CUSTOM_SURFACE_CONVERTER_H_
