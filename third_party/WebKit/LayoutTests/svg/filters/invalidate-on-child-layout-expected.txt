{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutSVGContainer g",
          "rect": [40, 40, 120, 120],
          "reason": "SVG resource change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [40, 40, 120, 120],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGContainer g",
          "rect": [0, 0, 110, 110],
          "reason": "SVG resource change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [0, 0, 110, 110],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGEllipse circle id='circle'",
          "rect": [50, 50, 100, 100],
          "reason": "full"
        },
        {
          "object": "LayoutSVGEllipse circle id='circle'",
          "rect": [0, 0, 100, 100],
          "reason": "full"
        }
      ]
    }
  ]
}

