{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutSVGContainer g",
          "rect": [6, 6, 158, 238],
          "reason": "style change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [6, 6, 158, 238],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [6, 186, 158, 58],
          "reason": "style change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [6, 126, 158, 58],
          "reason": "style change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [6, 66, 158, 58],
          "reason": "style change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [6, 6, 158, 58],
          "reason": "style change"
        }
      ]
    }
  ]
}

