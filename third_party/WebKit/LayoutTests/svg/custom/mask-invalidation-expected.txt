{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutSVGRect rect id='maskRect'",
          "rect": [100, 100, 460, 316],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRect rect id='maskRect'",
          "rect": [50, 50, 460, 316],
          "reason": "full"
        },
        {
          "object": "LayoutSVGContainer g id='root'",
          "rect": [50, 50, 453, 299],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [50, 50, 453, 299],
          "reason": "layoutObject removal"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [50, 50, 453, 299],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [100, 100, 403, 249],
          "reason": "layoutObject insertion"
        }
      ]
    }
  ]
}

