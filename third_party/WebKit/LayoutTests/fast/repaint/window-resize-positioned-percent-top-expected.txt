{
  "bounds": [600, 250],
  "children": [
    {
      "bounds": [600, 250],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 250, 600, 250],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV",
          "rect": [0, 250, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV",
          "rect": [0, 125, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "forced by layout"
        }
      ]
    }
  ]
}
{
  "bounds": [400, 250],
  "children": [
    {
      "bounds": [400, 250],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 400, 250],
          "reason": "full"
        },
        {
          "object": "LayoutView #document",
          "rect": [400, 0, 200, 250],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "forced by layout"
        }
      ]
    }
  ]
}
{
  "bounds": [400, 600],
  "children": [
    {
      "bounds": [400, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 400, 600],
          "reason": "full"
        },
        {
          "object": "LayoutView #document",
          "rect": [0, 250, 400, 350],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV",
          "rect": [0, 300, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV",
          "rect": [0, 125, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "forced by layout"
        }
      ]
    }
  ]
}
{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 800, 600],
          "reason": "full"
        },
        {
          "object": "LayoutView #document",
          "rect": [400, 0, 400, 600],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "forced by layout"
        }
      ]
    }
  ]
}

