{
  "bounds": [800, 2016],
  "children": [
    {
      "bounds": [800, 2016],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow (positioned) DIV class='absolute red'",
          "rect": [250, 230, 100, 100],
          "reason": "subtree"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV id='moveMe' class='absolute clipped'",
          "rect": [250, 230, 100, 100],
          "reason": "subtree"
        },
        {
          "object": "LayoutBlockFlow (relative positioned) DIV class='relative reflected'",
          "rect": [250, 230, 100, 100],
          "reason": "subtree"
        },
        {
          "object": "LayoutBlockFlow DIV class='green'",
          "rect": [250, 230, 100, 100],
          "reason": "subtree"
        },
        {
          "object": "LayoutReplica (anonymous)",
          "rect": [250, 230, 100, 100],
          "reason": "subtree"
        }
      ]
    }
  ]
}

