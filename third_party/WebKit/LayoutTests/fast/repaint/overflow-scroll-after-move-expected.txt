{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "children": [
        {
          "position": [10, 60],
          "bounds": [300, 400],
          "shouldFlattenTransform": false,
          "drawsContent": true,
          "children": [
            {
              "bounds": [285, 385],
              "shouldFlattenTransform": false,
              "children": [
                {
                  "bounds": [285, 900],
                  "drawsContent": true,
                  "paintInvalidations": [
                    {
                      "object": "LayoutBlockFlow (positioned) DIV id='block'",
                      "rect": [50, 310, 200, 50],
                      "reason": "bounds change"
                    },
                    {
                      "object": "LayoutBlockFlow (positioned) DIV id='block'",
                      "rect": [50, 200, 200, 50],
                      "reason": "bounds change"
                    }
                  ]
                }
              ]
            },
            {
              "bounds": [300, 400],
              "children": [
                {
                  "position": [0, 385],
                  "bounds": [285, 15]
                },
                {
                  "position": [285, 0],
                  "bounds": [15, 385],
                  "paintInvalidations": [
                    {
                      "object": "Vertical Scrollbar Layer",
                      "rect": [0, 0, 15, 385],
                      "reason": "full"
                    }
                  ]
                },
                {
                  "position": [285, 385],
                  "bounds": [15, 15],
                  "drawsContent": true
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}

