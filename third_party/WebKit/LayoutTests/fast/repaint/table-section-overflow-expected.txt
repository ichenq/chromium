{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [33, 58, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [33, 8, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [58, 58, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutTable TABLE",
          "rect": [58, 58, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutTableCell TD",
          "rect": [58, 58, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutTableRow TR",
          "rect": [58, 58, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutTableSection TBODY",
          "rect": [58, 58, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [58, 8, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutTable TABLE",
          "rect": [58, 8, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutTableCell TD",
          "rect": [58, 8, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutTableRow TR",
          "rect": [58, 8, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutTableSection TBODY",
          "rect": [58, 8, 50, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV id='spacer'",
          "reason": "became visible"
        }
      ]
    }
  ]
}

