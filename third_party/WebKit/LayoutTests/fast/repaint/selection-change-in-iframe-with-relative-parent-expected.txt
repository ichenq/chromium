{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutText #text",
          "rect": [18, 238, 40, 10],
          "reason": "selection"
        },
        {
          "object": "LayoutText #text",
          "rect": [18, 218, 30, 10],
          "reason": "selection"
        },
        {
          "object": "LayoutText #text",
          "rect": [18, 198, 30, 10],
          "reason": "selection"
        },
        {
          "object": "LayoutBR BR",
          "rect": [58, 238, 10, 10],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBR BR",
          "rect": [48, 218, 10, 10],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBR BR",
          "rect": [48, 198, 10, 10],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBR BR",
          "rect": [18, 228, 10, 10],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBR BR",
          "rect": [18, 208, 10, 10],
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "selection"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "selection"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "selection"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "selection"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "selection"
        },
        {
          "object": "InlineTextBox 'Bar'",
          "reason": "selection"
        },
        {
          "object": "InlineTextBox 'Bazz'",
          "reason": "selection"
        },
        {
          "object": "InlineTextBox 'Foo'",
          "reason": "selection"
        },
        {
          "object": "LayoutBR BR",
          "reason": "selection"
        },
        {
          "object": "LayoutBR BR",
          "reason": "selection"
        },
        {
          "object": "LayoutBR BR",
          "reason": "selection"
        },
        {
          "object": "LayoutBR BR",
          "reason": "selection"
        },
        {
          "object": "LayoutBR BR",
          "reason": "selection"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "reason": "selection"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "selection"
        }
      ]
    }
  ]
}

