{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutSVGInlineText #text",
          "rect": [103, 25, 160, 114],
          "reason": "outline"
        },
        {
          "object": "LayoutSVGText text",
          "rect": [103, 25, 160, 114],
          "reason": "full"
        },
        {
          "object": "LayoutSVGInlineText #text",
          "rect": [63, 25, 160, 114],
          "reason": "outline"
        },
        {
          "object": "LayoutSVGText text",
          "rect": [63, 25, 160, 114],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [118, 40, 130, 84],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [78, 40, 130, 84],
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'Foo'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'Foo'",
          "reason": "outline"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

