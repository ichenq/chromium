layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutSVGRoot {svg} at (50,50) size 200x200
    LayoutSVGHiddenContainer {defs} at (0,0) size 0x0
      LayoutSVGResourceMasker {mask} [id="mask"] [maskUnits=objectBoundingBox] [maskContentUnits=userSpaceOnUse]
        LayoutSVGEllipse {circle} at (150,50) size 100x100 [fill={[type=SOLID] [color=#FFFFFF]}] [cx=100.00] [cy=0.00] [r=50.00]
      LayoutSVGResourceClipper {clipPath} [id="clip"] [clipPathUnits=userSpaceOnUse]
        LayoutSVGEllipse {circle} at (50,150) size 100x100 [fill={[type=SOLID] [color=#000000]}] [cx=0.00] [cy=100.00] [r=50.00]
        LayoutSVGEllipse {circle} at (0,0) size 0x0 [fill={[type=SOLID] [color=#000000]}] [cx=0.00] [cy=0.00] [r=0.00]
    LayoutSVGEllipse {circle} at (150,50) size 100x100 [fill={[type=SOLID] [color=#FF0000]}] [cx=100.00] [cy=0.00] [r=50.00]
    LayoutSVGContainer {g} at (150,50) size 100x50
      [masker="mask"] LayoutSVGResourceMasker {mask} at (50,-50) size 100x70
      LayoutSVGRect {rect} at (0,0) size 300x100 [fill={[type=SOLID] [color=#008000]}] [x=-200.00] [y=-200.00] [width=400.00] [height=200.00]
    LayoutSVGEllipse {circle} at (50,150) size 100x100 [fill={[type=SOLID] [color=#FF0000]}] [cx=0.00] [cy=100.00] [r=50.00]
    LayoutSVGContainer {g} at (100,150) size 50x100
      [clipPath="clip"] LayoutSVGResourceClipper {clipPath} at (-50,50) size 100x100
      LayoutSVGRect {rect} at (100,0) size 200x300 [fill={[type=SOLID] [color=#008000]}] [x=0.00] [y=-200.00] [width=200.00] [height=400.00]
