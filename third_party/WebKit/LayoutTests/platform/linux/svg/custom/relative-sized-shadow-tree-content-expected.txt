{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow div id='contentBox'",
          "rect": [8, 72, 402, 402],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [109, 73, 300, 400],
          "reason": "incremental"
        },
        {
          "object": "LayoutSVGContainer use",
          "rect": [209, 273, 200, 200],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect id='targetRect'",
          "rect": [209, 273, 200, 200],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect id='targetRect'",
          "rect": [9, 273, 200, 200],
          "reason": "full"
        },
        {
          "object": "LayoutSVGContainer g id='targetUse'",
          "rect": [9, 73, 200, 200],
          "reason": "full"
        },
        {
          "object": "LayoutSVGContainer use",
          "rect": [9, 73, 200, 200],
          "reason": "full"
        },
        {
          "object": "LayoutSVGContainer use id='targetUse'",
          "rect": [9, 73, 200, 200],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRect rect id='targetRect1'",
          "rect": [9, 73, 200, 200],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRect rect id='targetRect1'",
          "rect": [9, 73, 200, 200],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRect rect id='targetRect1'",
          "rect": [9, 73, 200, 200],
          "reason": "full"
        },
        {
          "object": "LayoutSVGContainer use",
          "rect": [59, 273, 50, 200],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect id='targetRect'",
          "rect": [59, 273, 50, 200],
          "reason": "bounds change"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

