{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow div id='contentBox'",
          "rect": [8, 72, 402, 402],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutSVGRect rect id='targetRect'",
          "rect": [9, 73, 400, 400],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [109, 73, 300, 400],
          "reason": "incremental"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

