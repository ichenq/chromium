{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='inner-editor'",
          "rect": [10, 11, 150, 16],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutText #text",
          "rect": [10, 11, 35, 16],
          "reason": "full"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV id='div'",
          "rect": [8, 288, 10, 20],
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox ''",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'PASS'",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

