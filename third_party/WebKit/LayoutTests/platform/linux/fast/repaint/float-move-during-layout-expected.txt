{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='t'",
          "rect": [8, 110, 784, 100],
          "reason": "became visible"
        },
        {
          "object": "LayoutBlockFlow (floating) DIV",
          "rect": [8, 210, 100, 100],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow (floating) DIV",
          "rect": [8, 110, 100, 100],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow DIV id='s'",
          "reason": "bounds change"
        }
      ]
    }
  ]
}

