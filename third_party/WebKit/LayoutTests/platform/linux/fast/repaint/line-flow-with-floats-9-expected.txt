{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow P",
          "rect": [8, 80, 418, 519],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutBlockFlow (floating) SPAN id='yellowFloat'",
          "rect": [372, 243, 48, 49],
          "reason": "layoutObject insertion"
        },
        {
          "object": "LayoutText #text",
          "rect": [420, 240, 1, 139],
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox ' twist itself round and look up in her'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'Alice began to feel very uneasy: to be sure, she had not as\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'The chief difficulty Alice found at first was in managing her'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'The players all played at once without waiting\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'They\u2019re dreadfully fond of beheading people here; the great'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'a very short time '",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'any dispute with the Queen, but she knew that it might\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'away: besides all\n'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'begin again, it was very provoking to find that the'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'begin again, it was very provoking to find that the'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'besides all\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'blow with its head, it\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'bursting out\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'but generally, just as she had got its neck nicely'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'comfortably enough, under her arm, with its legs hanging'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'difficult game indeed.\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'doubled-up\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'down,\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'face, with\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'flamingo: she succeeded in getting its body tucked away,'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'for'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'going to\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'happen any'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hedgehog had\n'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'hedgehog had\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hedgehogs; and in\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'her head!\u2019 about'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'in the way wherever\n'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'laughing: and when she had got its head down, and was'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'me?'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'minute, \u2018and then,\u2019 thought she, \u2018what would become of\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'once in a minute.\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'other parts of\n'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'other parts of\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'out, and was going to give the hedgehog a'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'quarrelling all the while, and fighting for the'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'she wanted to send the hedgehog to, and, as the'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'she wanted to send the hedgehog to, and, as'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'shouting \u2018Off with his head!\u2019 or \u2018Off with\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'soldiers were always getting up and walking off to'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'soldiers were always getting up and walking off to'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'stamping about, and'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'straightened\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'such a puzzled expression that she could not help'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'the Queen'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'the doubled-up\n'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'the ground, Alice soon came to the conclusion that it'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'the ground, Alice soon came to the conclusion that it'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'this, there was generally a ridge or furrow in the way'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'this, there was generally a ridge or furrow'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'turns,\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'unrolled itself, and was in the act of crawling away:'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'unrolled itself, and was in the act of crawling'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'was a very\n'",
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'was a very\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'was in a furious passion, and went\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'wherever\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'wonder is, that there\u2018s any one left alive!\u2019'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'would'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'yet had'",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

