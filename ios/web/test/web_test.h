// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef IOS_WEB_TEST_WEB_TEST_H_
#define IOS_WEB_TEST_WEB_TEST_H_

// TODO(crbug.com/619076): Remove this file once downsteam clients stop using it

#import "ios/web/public/test/web_test_with_web_state.h"
#import "ios/web/test/web_test_with_web_controller.h"

#endif  // IOS_WEB_TEST_WEB_TEST_H_
